## WPezClasses: WPezAutoload

__Official autoloader for WPezClasses__


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

This is the folder structure you want to create

    |--- wp-content
    |    | --- mu-plugins
    |          |--- WPezSuite
    |               |--- WPezClasses
    |                    |--- WPezAutoload
    |                         |--- ClassWPezAutoload.php
    |                    |--- SomeOtherWPezClassesFolder
    |                    |--- wpezclasses.php


Within the WPezAutoload folder is a child folder named move-this, and within that a file wpezclasses.php. _Note: These are not in the diagram above._

Move wpezclasses.php out from under WPezAutoload/move-this, and place it under WPezClasses such that wpezclasses.php is a sibling to all the other folders within WPezClasses.

Then in the .php file in the mu-plugins folder that you're using to require / include your various mu-plugins files, add wpezclasses.php.



### FAQ

__1) Why?__

In its heart of hearts WPezClasses wants to be part of WP Core. Using mu-plugins and an autoloader creates the same effect. 


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://codex.wordpress.org/Must_Use_Plugins



### TODO

n/a


### CHANGE LOG


- v0.0.1 - Monday 15 April 2019
   
   - Hey! Ho!! Let's go!!! Another tool that's been the toolbox but not properly repo'ed. Please pardon the delay.