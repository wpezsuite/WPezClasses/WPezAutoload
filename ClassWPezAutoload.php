<?php

namespace WPezSuite\WPezClasses;

if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassWPezAutoload{

	protected $_str_needle;
	protected $_str_replace_search;
	protected $_str_replace_replace;

	protected $_version;
	protected $_url;
	protected $_path;
	protected $_path_parent;
	protected $_basename;
	protected $_file;


	public function __construct( $arr_args = [] ) {

		$this->setPropertyDefaults();

		$this->splAutoload();
	}

	/**
	 *
	 */
	protected function setPropertyDefaults() {

		$this->_str_needle              = __NAMESPACE__;
		$this->_str_replace_search  = 'TODO';
		$this->_str_replace_replace = '';

		$this->_version     = '0.5.0';
		$this->_url         = plugin_dir_url( __FILE__ );
		$this->_path        = plugin_dir_path( __FILE__ );
		$this->_path_parent = dirname( $this->_path );
		$this->_basename    = plugin_basename( __FILE__ );
		$this->_file        = __FILE__;
	}

	/** The class name should start with what?
	 * @param bool $str
	 */
	public function setNeedle( $str = false ){

		if ( is_string($str) ){
			$this->_str_needle = $str;
		}
	}

	public function setPathParent( $str = false ){

		if ( is_string($str) ){
			$this->_path_parent = $str;
		}
	}

	/**
	 * Trim what off the front of the file / class?
	 *
	 * @param bool $str
	 */
	public function setReplaceSearch( $str = false ){

		if ( is_string($str) ){
			$this->_str_replace_search = $str;
		}
	}

	public function setReplaceReplace( $str = false ){

		if ( is_string($str) ){
			$this->_str_replace_replace = $str;
		}
	}

	protected function splAutoload() {

		spl_autoload_register( null, false );
		spl_autoload_extensions( '.php' );
		//  spl_autoload_register( [ $this, 'wpezautoload' ] );
	}


	/**
	 * (@link http://www.phpro.org/tutorials/SPL-Autoload.html)
	 */
	public function WPezAutoload( $str_class ) {

		// needle
		if ( strpos( $str_class, $this->_str_needle ) !== 0 ) {
			return;
		}

		$arr_class = explode( '\\', $str_class );
		$str_file  = implode( DIRECTORY_SEPARATOR, $arr_class );
		// replace_search
		$str_file  = str_replace( $this->_str_replace_search, $this->_str_replace_replace, $str_file );
		// bring it all together
		$str_file  = $this->_path_parent . DIRECTORY_SEPARATOR . $str_file . '.php';

		if ( file_exists( $str_file ) ) {
			require $str_file;

			return true;
		}

		return false;
	}
}