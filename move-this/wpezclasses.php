<?php

namespace WPezSuite\WPezClasses;

function autoloader( $bool = true ){

	if ( $bool !== true ) {
		return;
	}

	require_once 'WPezAutoload/ClassWPezAutoload.php';

	$new_autoload = new ClassWPezAutoload();
	$new_autoload->setPathParent( dirname( __FILE__ ) );
	$new_autoload->setNeedle(__NAMESPACE__ );
	$new_autoload->setReplaceSearch(__NAMESPACE__ . DIRECTORY_SEPARATOR);

	spl_autoload_register( [$new_autoload, 'WPezAutoload'], true );
}
autoloader();
